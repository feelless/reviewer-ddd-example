package example.reviewer.application.reviewer;

import example.reviewer.domain.reviewer.Reviewer;
import example.reviewer.domain.reviewer.ReviewerRepository;
import example.reviewer.infrastructure.reviewer.ReviewerRepositoryImp;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ReviewerQService {

	@GetMapping(value = "/reviewer")
	public ResponseEntity<List<Reviewer>> findReviewers(@RequestParam(required = false) Integer limit) {
		ReviewerRepository rr = new ReviewerRepositoryImp();
		List<Reviewer> reviewers;

		if (limit != null) {
			reviewers = rr.findReviewersByLimit(limit);
		} else {
			reviewers = rr.findReviewers();
		}

		return new ResponseEntity<>(reviewers, HttpStatus.OK);
	}

	@GetMapping(value = "/reviewer/{id}")
	public ResponseEntity<Reviewer> findReviewer(@PathVariable String id) {
		ReviewerRepository rr = new ReviewerRepositoryImp();
		Reviewer reviewer = rr.findReviewerByID(id);
		return (reviewer == null) ? new ResponseEntity<>(HttpStatus.NOT_FOUND) : new ResponseEntity<>(reviewer, HttpStatus.OK);
	}


}


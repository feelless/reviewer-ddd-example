package example.reviewer.application.reviewer;


import example.reviewer.domain.reviewer.Reviewer;
import example.reviewer.infrastructure.reviewer.ReviewerRepositoryImp;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ReviewerCService {

  @PostMapping(value = "/reviewer")
  public ResponseEntity addReviewer(@RequestBody Reviewer reviewer) {
    ReviewerRepositoryImp rr = new ReviewerRepositoryImp();
    if (!reviewer.isValidate()) return new ResponseEntity(HttpStatus.BAD_REQUEST);
    rr.addReviewer(reviewer);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }

  @DeleteMapping(value = "/reviewer/{id}")
  public ResponseEntity deleteReviewer(@PathVariable String id) {

    return new ResponseEntity(HttpStatus.OK);
  }

}

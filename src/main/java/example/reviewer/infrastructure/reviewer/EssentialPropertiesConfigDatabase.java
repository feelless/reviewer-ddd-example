package example.reviewer.infrastructure.reviewer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EssentialPropertiesConfigDatabase {

  static String host;

  static int port;

  static String collection;

  static String database;

  @Value("${spring.data.mongodb.host}")
  public void setHost(String host) {
    EssentialPropertiesConfigDatabase.host = host;
  }

  @Value("${spring.data.mongodb.port}")
  public void setPort(int port) {
    EssentialPropertiesConfigDatabase.port = port;
  }

  @Value("${spring.session.mongodb.collection-name}")
  public void setCollection(String collection) {
    EssentialPropertiesConfigDatabase.collection = collection;
  }

  @Value("${spring.data.mongodb.database}")
  public void setDatabase(String database) {
    EssentialPropertiesConfigDatabase.database = database;
  }

}

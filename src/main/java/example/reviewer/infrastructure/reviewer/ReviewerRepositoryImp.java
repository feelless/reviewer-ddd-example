package example.reviewer.infrastructure.reviewer;

import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import example.reviewer.domain.reviewer.Reviewer;
import example.reviewer.domain.reviewer.ReviewerRepository;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class ReviewerRepositoryImp extends EssentialPropertiesConfigDatabase implements ReviewerRepository {

  @Override
  public void addReviewer(Reviewer reviewer) {

    try (MongoClient mc = new MongoClient(host, port)) {
      MongoDatabase md = mc.getDatabase(database);
      MongoCollection<Document> mongoCollection = md.getCollection(collection);
      String reviewerJson = new Gson().toJson(reviewer);
      mongoCollection.insertOne(Document.parse(reviewerJson));
    }

  }

  @Override
  public void deleteReviewer(String key) {

  }

  @Override
  public List<Reviewer> findReviewersByLimit(int limit) {

    try (MongoClient mc = new MongoClient(host, port)) {

      MongoDatabase md = mc.getDatabase(database);
      MongoCollection<Document> mongoCollection = md.getCollection(collection);
      FindIterable<Document> fi = mongoCollection.find().limit(limit);

      return toList(fi.iterator());
    }

  }

  @Override
  public Reviewer findReviewerByKey(String key) {
    return null;
  }

  @Override
  public Reviewer findReviewerByID(String id) {

    Gson gson = null;
    Document document = null;

    try (MongoClient mc = new MongoClient(host, port)) {
      MongoDatabase md = mc.getDatabase(database);
      MongoCollection<Document> mongoCollection = md.getCollection(collection);
      document = mongoCollection.find(Filters.eq("reviewerID", id)).first();
      gson = new Gson();
    }

    return (document != null) ? gson.fromJson(document.toJson(), Reviewer.class) : null;
  }

  @Override
  public List<Reviewer> findReviewers() {

    try (MongoClient mc = new MongoClient(host, port)) {

      MongoDatabase md = mc.getDatabase(database);
      MongoCollection<Document> mongoCollection = md.getCollection(collection);
      FindIterable<Document> fi = mongoCollection.find();

      return toList(fi.iterator());
    }
  }

  private List<Reviewer> toList(MongoCursor<Document> mc) {
    List<Reviewer> reviewers = new ArrayList<>();

    Gson gson = new Gson();
    while (mc.hasNext()) {
      Reviewer reviewer = gson.fromJson(mc.next().toJson(), Reviewer.class);
      reviewers.add(reviewer);
    }

    mc.close();
    return reviewers;
  }


}

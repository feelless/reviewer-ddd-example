package example.reviewer.domain.reviewer;

import java.text.MessageFormat;

public class EventInTime {

  private String timestamp;
  public EventInTime(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getTimestamp() {
    return timestamp;
  }

  @Override
  public String toString() {
    return MessageFormat.format("eventInTime: [ timestamp: {0} ]", timestamp);
  }
}

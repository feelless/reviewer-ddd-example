package example.reviewer.domain.reviewer;

import java.text.MessageFormat;

public class Product {
 private String productID;
 private String productName;

 public Product(String productID, String productName){
   this.productID = productID;
   this.productName = productName;
 }

 /**
  * @return the productID
  */
 public String getProductID() {
     return productID;
 }

 /**
  * @param productID the productID to set
  */
 public void setProductID(String productID) {
     this.productID = productID;
 }

 /**
  * @return the productName
  */
 public String getProductName() {
     return productName;
 }

 /**
  * @param productName the productName to set
  */
 public void setProductName(String productName) {
     this.productName = productName;
 }

 @Override
 public String toString(){

   return MessageFormat.format("product: [ productID: {0}, productName: {1} ]", productID, productName);
 }
}
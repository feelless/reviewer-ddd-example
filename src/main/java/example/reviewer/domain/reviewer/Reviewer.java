package example.reviewer.domain.reviewer;

import java.text.MessageFormat;

public class Reviewer {

  /* *
   * ต้องการเก็บ event จากผู้ใช้ที่ทำการ review product
   *  Business rules
   *  - เก็บ id ของผู้ที่ review
   *  - เก็บ timestamp ตอนที่ผู้ใช้กด submit review
   *  - เก็บ id ของ product ที่ผู้ใข้กำการ review
   * */

  private String reviewerID;
  private User user;
  private EventInTime eventInTime;

  public Reviewer( String reviewerID, User user, EventInTime eventInTime) {
    this.reviewerID = reviewerID;
    this.user = user;
    this.eventInTime = eventInTime;
  }

  public void setEventInTime(EventInTime eventInTime) {
    this.eventInTime = eventInTime;
  }

  public void setReviewerID(String reviewerID) {
    this.reviewerID = reviewerID;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public User getUser() {
    return user;
  }

  public EventInTime getEventInTime() {
    return eventInTime;
  }

  public String getReviewerID() {
    return reviewerID;
  }

  public boolean isValidate() {
    return true;
  }

  @Override
  public String toString() {
    return MessageFormat.format("reviewer: [ reviewerID: {0}, {1}, {2} ]", reviewerID, user, eventInTime);
  }

}


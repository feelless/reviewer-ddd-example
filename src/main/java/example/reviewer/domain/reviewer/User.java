package example.reviewer.domain.reviewer;

import java.text.MessageFormat;

public class User {

  private  String userID;
  private Product product;

  public User(String userID, Product product) {
    this.userID = userID;
    this.product = product;
  }

  public Product getProduct() {
    return product;
  }

  public String getUserID() {
    return userID;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public void setUserID(String userID) {
    this.userID = userID;
  }

  @Override
  public String toString(){
    return MessageFormat.format("user: [ userID: {0}, {1} ]", userID, product);
  }
}

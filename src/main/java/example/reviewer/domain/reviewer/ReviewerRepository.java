package example.reviewer.domain.reviewer;

import java.util.List;

public interface ReviewerRepository {

  void addReviewer(Reviewer reviewer);

  void deleteReviewer(String key);

  List<Reviewer> findReviewersByLimit(int limit);

  Reviewer findReviewerByKey(String key);

  Reviewer findReviewerByID(String id);

  List<Reviewer> findReviewers();

}

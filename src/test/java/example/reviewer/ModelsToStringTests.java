package example.reviewer;

import example.reviewer.domain.reviewer.EventInTime;
import example.reviewer.domain.reviewer.Product;
import example.reviewer.domain.reviewer.Reviewer;
import example.reviewer.domain.reviewer.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ModelsToStringTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void productModelToString() {
		String actual = "product: [ productID: 1, productName: melon ]";
		Product product = new Product("1", "melon");
		System.out.println(product);
		Assert.isTrue(product.toString().equals("#".replaceAll("#", actual)),
		 "should be #".replaceAll("#", actual));
	}

	@Test
	public void userModelToString() {
		String actual = "user: [ userID: 1, product: [ productID: 1, productName: melon ] ]";
		User user = new User( "1", new Product( "1", "melon") );
		System.out.println(user);
		Assert.isTrue(user.toString().equals("#".replaceAll("#", actual)),
			"should be #".replaceAll("#", actual));
	}

	@Test
	public void  eventInTimeModelToString() {
		String actual = "eventInTime: [ timestamp: 2019-08-31 01:05:20 ]";
		EventInTime eit = new EventInTime("2019-08-31 01:05:20");
		System.out.println(eit);
		Assert.isTrue(eit.toString().equals("#".replaceAll("#", actual)),
			"should be #".replaceAll("#", actual));
	}

	@Test
	public void reviewerModelToString() {
		String actual = "reviewer: [ reviewerID: 1, user: [ userID: 1, product: [ productID: 1, productName: melon ] ], " +
			"eventInTime: [ timestamp: 2019-08-31 01:05:20 ] ]";
		Reviewer reviewer = new Reviewer(
				"1",
				new User(
					"1",
					 new Product( "1", "melon")
				),
				new EventInTime("2019-08-31 01:05:20")
			);
		System.out.println(reviewer);
		Assert.isTrue(reviewer.toString().equals("#".replaceAll("#", actual)),
			"should be #".replaceAll("#", actual));
	}

}

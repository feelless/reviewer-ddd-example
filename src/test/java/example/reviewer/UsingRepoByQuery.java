package example.reviewer;

import example.reviewer.domain.reviewer.Reviewer;
import example.reviewer.domain.reviewer.ReviewerRepository;
import example.reviewer.infrastructure.reviewer.ReviewerRepositoryImp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsingRepoByQuery {

  @Test
  public void whenFindReviewersShouldNotNull() {
    ReviewerRepository rr = new ReviewerRepositoryImp();
    List<Reviewer> reviewers = rr.findReviewers();
    Assert.notNull(reviewers, "reviewer should not null");
  }

  @Test
  public void whenFindReviewersShouldMustThanZero() {
    ReviewerRepository rr = new ReviewerRepositoryImp();
    List<Reviewer> reviewers = rr.findReviewers();
    Assert.isTrue(reviewers.size() > 0, "please add some data into Database");
  }
}

package example.reviewer;

import example.reviewer.domain.reviewer.*;
import example.reviewer.infrastructure.reviewer.ReviewerRepositoryImp;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsingRepoByCommand {

  @Test
  public void addReviewer() {

    try {
      ReviewerRepository reviewerRepository = new ReviewerRepositoryImp();
      Reviewer reviewer = new Reviewer(
        "1",
        new User("1", new Product("1", "melon")),
        new EventInTime("2019-08-31 13:03:22")
      );

      reviewerRepository.addReviewer(reviewer);
      Assert.assertTrue("addReviewer successed", true);
    } catch (Exception exc) {
      Assert.fail(exc.getMessage());
    }
  }

}
